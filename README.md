ZR IRC
======

IRC for the Zombie Raptor game engine.

The [Zombie Raptor](https://zombieraptor.com/) game engine is a 3D
Common Lisp game engine. Due to the massive scope of that project,
many associated projects have been created. These projects might
become usable before the game engine itself is usable. This is one of
those projects.

[Internet Relay Chat
(IRC)](https://en.wikipedia.org/wiki/Internet_Relay_Chat) is a simple
group text chat protocol that has seen widespread implementation by
many different clients and bots in many different languages due to its
simplicity.

This project is an attempt to write a complete implementation of the
[IRC and IRCv3 protocols](https://ircv3.net/irc/) to provide text chat
functionality for the Zombie Raptor game engine. The game engine
should be able to use this library as part of an IRC module.

However, ZR IRC is general purpose. It can be used as a standalone
library for things such as IRC bots, IRC clients, and IRC daemons
(IRCd).

The plan is to first write an IRC bot framework, then to write an IRC
client on top of the Zombie Raptor game engine (while completing the
monospace font rendering there), and then to finally write an IRC
bouncer for that IRC client so that connections to servers can
persist.

The new IRC client will add new `CTCP` commands for various
turn-based, graphical games that can be played between other instances
of the client. For example, chess.

The official IRC channel is `#zombieraptor` on
[`irc.libera.chat`](https://libera.chat/) unless the IRC library
and/or programs become popular enough to need their own IRC channels.
