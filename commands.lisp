(cl:defpackage #:zr-irc/commands
  (:use #:cl
        #:zr-utils
        #:zr-irc/util)
  (:import-from #:zr-irc/log
                #:log-command)
  (:export #:command-queue-empty?
           #:ctcp
           #:irc-list
           #:join
           #:me
           #:msg
           #:names
           #:nctcp
           #:notice
           #:part
           #:quit
           #:run-commands-from-queue
           #:topic
           #:who
           #:whois
           #:write-command))

(cl:in-package #:zr-irc/commands)

;;;; Command queue

(define-struct-queue command-list ((list list) 10))

(defvar *queue* (make-command-list))

(define-function (command-queue-empty? :inline t) ()
  (command-list-empty? *queue*))

;;;; Message commands

(defun msg (target text)
  (command-list-enqueue *queue* `(:privmsg ,(format nil "~A :~A" target text)))
  nil)

(defun notice (target text)
  (command-list-enqueue *queue* `(:notice ,(format nil "~A :~A" target text)))
  nil)

(defun ctcp (target command &optional (text ""))
  (let ((*print-case* :upcase))
    (msg target (format nil "~A~A ~A~A" (code-char #x01) command text (code-char #x01)))))

(defun nctcp (target command &optional (text ""))
  (let ((*print-case* :upcase))
    (notice target (format nil "~A~A ~A~A" (code-char #x01) command text (code-char #x01)))))

(defun me (target text)
  (ctcp target :action text))

;;;; Other commands

(defun irc-list (&optional (channel ""))
  (command-list-enqueue *queue* `(:list ,channel))
  nil)

(defun join (channel-and-key)
  (command-list-enqueue *queue* `(:join ,channel-and-key))
  nil)

(defun names (target)
  (command-list-enqueue *queue* `(:names ,target))
  nil)

(defun part (target &optional reason)
  (command-list-enqueue *queue* `(:part ,(format nil "~A~@[ :~A~]" target reason)))
  nil)

(defun quit (&optional (message ""))
  (command-list-enqueue *queue* `(:quit ,(format nil ":~A" message)))
  nil)

(defun topic (channel &optional topic)
  (command-list-enqueue *queue* `(:topic ,(format nil "~A~@[ :~A~]" channel topic)))
  nil)

(defun who (target)
  (command-list-enqueue *queue* `(:who ,target))
  nil)

(defun whois (target)
  (command-list-enqueue *queue* `(:whois ,target))
  nil)

;;;; Run commands

(defun write-command (command parameters connection log user)
  (log-command command parameters user log :hide-ping? t :ignore-silent-commands? t)
  (let ((octets (sb-ext:string-to-octets (symbol-name command))))
    (write-sequence octets connection))
  (write-byte #x20 connection)
  (let ((octets (sb-ext:string-to-octets parameters)))
    (write-sequence octets connection)
    (write-byte #x0D connection)
    (write-byte #x0A connection))
  nil)

(defun run-commands-from-queue (user stream log)
  (loop :for in-command := (command-list-dequeue *queue*)
        :with quit? := nil
        :until (command-queue-empty?)
        :do
           (when (eql :quit (car in-command))
             (setf quit? t))
           (write-command (car in-command)
                          (cadr in-command)
                          stream
                          log
                          user)
        :finally
           (return
             (progn
               (when (eql :quit (car in-command))
                 (setf quit? t))
               (write-command (car in-command)
                              (cadr in-command)
                              stream
                              log
                              user)
               (force-output stream)
               quit?))))
