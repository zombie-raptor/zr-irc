(uiop:define-package #:zr-irc/tests
  (:use #:cl
        #:zr-utils)
  (:use-reexport #:zr-irc/tests/all
                 #:zr-irc/tests/log))
