(cl:defpackage #:zr-irc/log
  (:use #:cl
        #:zr-utils
        #:zr-irc/core
        #:zr-irc/log-file
        #:zr-irc/util)
  (:export #:log-command
           #:log-message))

(cl:in-package #:zr-irc/log)

(defun write-message-source (message
                           log
                           &key
                             (name-and-host? nil)
                             (space-separated? t)
                             (start-char #\[)
                             (end-char #\]))
  (with-irc-message (source-start source-split source-end string)
      message
    (write-string string log :start source-start :end (non-negative source-split))
    (when name-and-host?
      (when space-separated?
        (write-char #\Space log))
      (write-char start-char log)
      (unless (= source-split source-end)
        (write-string string log :start (1+ source-split) :end (non-negative source-end)))
      (write-char end-char log))))

(defun make-log-path (string start &optional end)
  (let* ((name-string (subseq string start end))
         (filename (merge-pathnames (uiop:subpathname (user-homedir-pathname)
                                                           #P"zr-irc-logs/"
                                                           :type :directory)
                                         (make-pathname :name name-string
                                                        :type "log"))))
    (create-log-if-necessary filename :open? t)
    (values name-string filename)))

(define-function write-ctcp ((message irc-message-type-2) (stream stream))
  (with-irc-message-type-2 (string
                            (a parameter-index-array))
      message
    (with-array-accessors* ((target-start  a :offset 0)
                            (target-end    a :offset 1)
                            (message-start a :offset 2))
      (let* ((separator (next-separator-position string message-start))
             (contents (next-parameter-start string separator))
             (trailing-ctcp-marker? (char= (code-char #x01)
                                           (aref string (1- (length string))))))
        (write-string string stream :start target-start :end (non-negative target-end))
        (write-char #\Space stream)
        (write-string string
                      stream
                      :start (1+ message-start)
                      :end (or separator
                               (if (char= (code-char #x01)
                                          (aref string (1- (length string))))
                                   (1- (length string))
                                   nil)))
        (write-char #\] stream)
        (when (and contents
                   (< contents
                      (- (length string) (if trailing-ctcp-marker? 1 0))))
          (write-char #\Space stream)
          (write-string string
                        stream
                        :start (1+ separator)
                        :end (if trailing-ctcp-marker?
                                 (1- (length string))
                                 nil)))))))

;;; TODO: Optionally track role (e.g. <@foo>) in channels for PRIVMSG.
;;; This requires state tracking for both commands and messages. In
;;; particular, RPL_ISUPPORT must be parsed for PREFIX so that it
;;; knows which PREFIX in NAMES or mode in MODE modifies the visible
;;; role.
(define-function write-privmsg ((message irc-message-type-2) (user client) (stream stream) &optional log-to-disk?)
  (with-irc-message-type-2 (string
                            source-start
                            source-split
                            source-end
                            (a parameter-index-array))
      message
    (with-array-accessors* ((target-start a :offset 0)
                            (target-end   a :offset 1))
      (when log-to-disk?
        (unless (minusp target-start)
          (format t "~A~%" (if (and (message-prefix-match? (client-nick user)
                                                           string
                                                           :suffix-char #\Space
                                                           :message-start target-start)
                                    source-start)
                               (make-log-path string source-start source-split)
                               (make-log-path string target-start target-end)))))))
  (with-irc-message-type-2 (string
                            ctcp?
                            (a parameter-index-array))
      message
    (with-array-accessors* ((target-start  a :offset 0)
                            (target-end    a :offset 1)
                            (message-start a :offset 2))
      (let ((ctcp-action? (and ctcp?
                               message-start
                               (message-prefix-match? "ACTION"
                                                      string
                                                      :message-start (1+ message-start)
                                                      :suffix-char #\Space))))
        (if (or (not ctcp?) ctcp-action?)
            (progn
              (if ctcp-action?
                  (write-string "* " stream)
                  (write-char #\< stream))
              (write-message-source message stream)
              (write-char #\: stream)
              (write-string string stream :start target-start :end (non-negative target-end))
              (unless ctcp-action?
                (write-char #\> stream))
              (write-char #\Space stream)
              (unless (minusp message-start)
                (write-string string
                              stream
                              :start (if ctcp-action?
                                         (+ message-start 1 (length "ACTION "))
                                         message-start)
                              :end (if (and ctcp-action?
                                            (char= (code-char #x01)
                                                   (aref string (1- (length string)))))
                                       (1- (length string))
                                       nil))))
            (progn
              (write-string "[->CTCP " stream)
              (write-message-source message stream)
              (write-string "->" stream)
              (write-ctcp message stream)))
        (terpri stream)))))

(define-function write-notice ((message irc-message-type-2)
                               incoming-message?
                               user
                               stream
                               &optional
                               log-to-disk?)
  (with-irc-message-type-2 (string
                            source-start
                            source-split
                            source-end
                            (a parameter-index-array))
      message
    (with-array-accessors* ((target-start a :offset 0)
                            (target-end   a :offset 1))
      (when log-to-disk?
        (unless (minusp target-start)
          (format t "~A~%" (if (and (message-prefix-match? (client-nick user)
                                                           string
                                                           :suffix-char #\Space
                                                           :message-start target-start)
                                    source-start)
                               (make-log-path string source-start source-split)
                               (make-log-path string target-start target-end)))))))
  (with-irc-message-type-2 (string
                            ctcp?
                            source-split
                            source-end
                            (a parameter-index-array))
      message
    (with-array-accessors* ((target-start  a :offset 0)
                            (target-end    a :offset 1)
                            (message-start a :offset 2))
      (let ((channel-message? (string/= (client-nick user)
                                        string
                                        :start2 target-start
                                        :end2 (non-negative target-end))))
        (cond (ctcp?
               (write-string "[<-CTCP " stream)
               (write-message-source message stream)
               (write-string "->" stream)
               (write-ctcp message stream))
              (channel-message?
               (write-char #\- stream)
               (write-message-source message stream)
               (write-char #\: stream)
               (write-string string stream :start target-start :end (non-negative target-end))
               (write-string "- " stream)
               (unless (minusp message-start)
                 (write-string string stream :start message-start)))
              ((and incoming-message? (/= source-split source-end))
               (write-char #\- stream)
               (write-message-source message
                                     stream
                                     :name-and-host? t
                                     :start-char #\(
                                     :end-char #\)
                                     :space-separated? nil)
               (write-string "- " stream)
               (unless (minusp message-start)
                 (write-string string stream :start message-start)))
              (t
               (write-char #\! stream)
               (write-message-source message stream)
               (write-char #\Space stream)
               (unless (minusp message-start)
                 (write-string string stream :start message-start))))
        (terpri stream)))))

(define-function write-ping ((message irc-message-type-2) (stream stream))
  (with-irc-message-type-2 (string (a parameter-index-array)) message
    (with-array-accessors* ((source-start      a :offset 0)
                            (source-end        a :offset 1)
                            (destination-start a :offset 2))
      (write-string "-!- [PING] " stream)
      (write-string string stream :start source-start :end (non-negative source-end))
      ;; Note: If the destination isn't provided, then the
      ;; source is assumed to be the destination.
      (unless (minusp destination-start)
        (write-char #\Space stream)
        (write-string string stream :start destination-start))
      (terpri stream))))

(define-function write-join ((message irc-message-type-1) (stream stream))
  (with-irc-message-type-1 (string (a parameter-index-array)) message
    (with-array-accessors* ((channel-start a :offset 0))
      (write-string "--> " stream)
      (write-message-source message stream :name-and-host? t)
      (write-string " has joined " stream)
      (write-string string stream :start channel-start)
      (terpri stream))))

(define-function write-part ((message irc-message-type-2) (stream stream))
  (with-irc-message-type-2 (string (a parameter-index-array)) message
    (with-array-accessors* ((channel-start a :offset 0)
                            (channel-end   a :offset 1)
                            (reason-start  a :offset 2))
      (write-string "<-- " stream)
      (write-message-source message stream :name-and-host? t)
      (write-string " has left " stream)
      (write-string string stream :start channel-start :end (non-negative channel-end))
      (write-string " [" stream)
      (unless (minusp reason-start)
        (write-string string stream :start reason-start))
      (write-char #\] stream)
      (terpri stream))))

(define-function write-kick ((message irc-message-type-3) (stream stream))
  (with-irc-message-type-3 (string (a parameter-index-array)) message
    (with-array-accessors* ((channel-start a :offset 0)
                            (channel-end   a :offset 1)
                            (target-start  a :offset 2)
                            (target-end    a :offset 3)
                            (reason-start  a :offset 4))
      (cond ((minusp channel-start)
             (write-line "-!- Invalid KICK message: no channel provided" stream))
            ((minusp target-start)
             (write-line "-!- Invalid KICK message: no target nick provided" stream))
            (t
             (write-string "<-- " stream)
             (write-string string stream :start target-start :end (non-negative target-end))
             (write-string " was kicked from " stream)
             (write-string string stream :start channel-start :end (non-negative channel-end))
             (write-string " by " stream)
             (write-message-source message stream)
             (write-string " [" stream)
             (unless (minusp reason-start)
               (write-string string stream :start reason-start))
             (write-char #\] stream)
             (terpri stream))))))

(define-function write-quit ((message irc-message-type-1) (stream stream))
  (with-irc-message-type-1 (string (a parameter-index-array)) message
    (with-array-accessors* ((quit-message-start a :offset 0))
      (write-string "<-- " stream)
      (write-message-source message stream :name-and-host? t)
      (write-string " has quit [" stream)
      (unless (minusp quit-message-start)
        (write-string string stream :start quit-message-start))
      (write-char #\] stream)
      (terpri stream))))

(define-function write-nick ((message irc-message-type-1) (stream stream))
  (with-irc-message-type-1 (string (a parameter-index-array)) message
    (with-array-accessors* ((new-nick-start a :offset 0))
      (write-string "-!- " stream)
      (write-message-source message stream)
      (write-string " is now known as " stream)
      (if (minusp new-nick-start)
          (write-string "<message parse error>" stream)
          (write-string string stream :start new-nick-start))
      (terpri stream))))

(define-function write-mode ((message irc-message-type-2) (stream stream))
  (with-irc-message-type-2 (string (a parameter-index-array)) message
    (with-array-accessors* ((target-start a :offset 0)
                            (target-end   a :offset 1)
                            (modes-start  a :offset 2))
      (write-string "-!- mode/" stream)
      (write-string string stream :start target-start :end (non-negative target-end))
      (write-string " [" stream)
      (unless (minusp modes-start)
        (write-string string
                      stream
                      :start modes-start
                      ;; TODO: don't print any trailing or leading whitespace
                      :end (if (char= #\Space (aref string (1- (length string))))
                               (1- (length string)))))
      (write-string "] by " stream)
      (write-message-source message stream)
      (terpri stream))))

(define-function write-topic ((message irc-message-type-2) (stream stream))
  (with-irc-message-type-2 (string (a parameter-index-array)) message
    (with-array-accessors* ((channel-start a :offset 0)
                            (channel-end   a :offset 1)
                            (topic-start   a :offset 2))
      (write-string "-!- " stream)
      (write-message-source message stream)
      (write-string " set the topic of " stream)
      (write-string string stream :start channel-start :end (non-negative channel-end))
      (write-string " to: " stream)
      (if (minusp topic-start)
          (write-string "an unknown topic (invalid TOPIC message)" stream)
          (write-string string stream :start topic-start))
      (terpri stream))))

(define-function write-invite ((message irc-message-type-2)
                               (user client)
                               (stream stream))
  (with-irc-message-type-2 (string (a parameter-index-array)) message
    (with-array-accessors* ((invitee-start a :offset 0)
                            (invitee-end   a :offset 1)
                            (channel-start a :offset 2))
      (write-string "-!- " stream)
      (write-message-source message stream)
      (write-string " invites " stream)
      (if (string= (client-nick user)
                   string
                   :start2 invitee-start
                   :end2 (non-negative invitee-end))
          (write-string "you" stream)
          (write-string string stream :start invitee-start :end (non-negative invitee-end)))
      (write-string " to " stream)
      (if (minusp channel-start)
          (write-string "an unknown channel (invalid INVITE message)" stream)
          (write-string string stream :start channel-start))
      (terpri stream))))

;;; If it doesn't recognize the message, write it as it appears.
(define-function write-unknown-message ((message irc-message) stream)
  (with-irc-message (string) message
    (write-line string stream)
    nil))

(define-function write-irc-numeric ((number uint16) (message irc-message) (user client) stream log-to-disk?)
  (write-time :stream stream)
  (write-char #\Space stream)
  (with-irc-message (source-start source-end parameters string)
      message
    (case= number
      ;; RPL_TOPIC
      (332
       ;; <client> <channel> <topic>
       (multiple-value-bind (client-start client-end channel-start channel-end topic-start)
           (three-parameter-message string parameters)
         (declare (ignore client-start client-end))
         (when log-to-disk?
           (format t "~A~%" (make-log-path string channel-start channel-end)))
         (write-string "-!- " stream)
         (write-string string stream :start channel-start :end (non-negative channel-end))
         (write-string " topic: " stream)
         (if topic-start
             (write-string string stream :start topic-start)
             (write-string "an unknown topic (invalid 332 response)" stream))))
      ;; RPL_TOPICWHOTIME
      (333
       ;; <client> <channel> <setter-nick> <set-timestamp>
       (multiple-value-bind (client-start client-end channel-start channel-end setter-nick-start setter-nick-end timestamp-start)
           (four-parameter-message string parameters)
         (declare (ignore client-start client-end))
         (when log-to-disk?
           (format t "~A~%" (make-log-path string channel-start channel-end)))
         (let ((setter-nick-split (and setter-nick-start
                                       (or (position #\!
                                                     string
                                                     :start setter-nick-start
                                                     :end (non-negative setter-nick-end))
                                           setter-nick-end))))
           (write-string "-!- Topic set by " stream)
           (when setter-nick-start ; unless (minusp setter-nick-start)
             (write-string string stream :start setter-nick-start :end (non-negative setter-nick-split))
             (write-string " [" stream)
             (unless (= setter-nick-end setter-nick-split)
               (write-string string stream :start setter-nick-split :end (non-negative setter-nick-end)))
             (write-string "] [" stream)
             (when timestamp-start   ; unless (minusp timestamp-start)
               (let ((timestamp (parse-integer string :start timestamp-start)))
                 (write-datetime :stream stream :unix-time? t :encoded-time timestamp)))
             (write-char #\] stream)))))
      ;; RPL_NOTOPIC
      (331
       ;; <client> <channel> <error-message>
       (multiple-value-bind (client-start client-end channel-start channel-end error-message-start)
           (three-parameter-message string parameters)
         (declare (ignore client-start client-end error-message-start))
         (when log-to-disk?
           (format t "~A~%" (make-log-path string channel-start channel-end)))
         (write-string "-!- No topic is set in " stream)
         (write-string string stream :start channel-start :end (non-negative channel-end))))
      ;; RPL_INVITING
      (341
       ;; <client> <invitee-nick> <channel>
       (multiple-value-bind (client-start client-end invitee-start invitee-end channel-start)
           (three-parameter-message string parameters)
         (declare (ignore client-start client-end))
         (when channel-start           ; unless (minusp channel-start)
           (when log-to-disk?
             (format t "~A~%" (make-log-path string channel-start nil))))
         (write-string string stream :start invitee-start :end (non-negative invitee-end))
         (write-string " has been invited to " stream)
         (if channel-start       ; reverse with (minusp channel-start)
             (write-string string stream :start channel-start)
             (write-string "an unknown channel (invalid 341 response)" stream))))
      ;; RPL_BANLIST
      ;;
      ;; Note: This differs from the horse documentation
      (367
       ;; <client> <channel> <mask> [<ban-by> [<timestamp>]]
       (multiple-value-bind (channel-start channel-end mask-start mask-end by-start by-end time-start)
           ;; Skip the ignored client parameter so that there are
           ;; only four parameters.
           (four-parameter-message string (next-separator-position string parameters))
         (when log-to-disk?
           (format t "~A~%" (make-log-path string channel-start channel-end)))
         (write-string "-!- [ban] " stream)
         (write-string string stream :start channel-start :end (non-negative channel-end))
         (write-string ": " stream)
         (write-string string stream :start mask-start :end (non-negative mask-end))
         (write-string " [" stream)
         (when by-start                 ; unless (minusp by-start)
           (write-string " by ")
           (write-string string stream :start by-start :end (non-negative by-end)))
         (write-string "] [" stream)
         (when time-start               ; unless (minusp time-start)
           (let ((timestamp (parse-integer string :start time-start)))
             (write-datetime :stream stream :unix-time? t :encoded-time timestamp)))
         (write-char #\] stream)))
      ;; RPL_ENDOFBANLIST
      (368
       ;; <client> <channel> <end-message>
       (multiple-value-bind (client-start client-end channel-start channel-end end-message)
           (three-parameter-message string parameters)
         (declare (ignore client-start client-end))
         (write-string "-!- [ban] " stream)
         (write-string string stream :start channel-start :end (non-negative channel-end))
         (when end-message              ; unless (minusp end-message)
           (write-string ": " stream)
           (write-string string
                         stream
                         :start end-message))))
      (t
       (when source-start               ; unless (minusp source-start)
         (when log-to-disk?
           (format t "~A~%" (make-log-path string source-start source-end))))
       (write-string "-!- " stream)
       ;; For certain numbers, don't use fancy formatting. Just
       ;; change the prefix from the number code into the action.
       (cond ((<= 321 number 323)
              ;; RPL_LISTSTART (321) RPL_LIST (322) RPL_LISTEND (323)
              (write-string "[LIST] " stream))
             ((or (= 352 number)
                  (= 315 number))
              ;; RPL_WHOREPLY (352) RPL_ENDOFWHO (315)
              (write-string "[WHO] " stream))
             (t
              (format stream "[~3,'0D] " number)))
       (let ((trail (let ((trail* (search " :"
                                          string
                                          :start2 parameters)))
                      (if trail*
                          (+ 2 trail*)
                          nil))))
         (cond ((message-prefix-match? (client-nick user)
                                       string
                                       :message-start parameters
                                       :suffix-char #\Space)
                (write-string string
                              stream
                              :start (+ parameters (length (client-nick user)) 1)
                              :end (if trail (- trail 1) nil))
                (when trail
                  (write-string string stream :start trail)))
               (t
                (write-string string stream :start parameters))))))
    (terpri stream)))

(define-function write-named-message ((command keyword) (message irc-message) (user client) stream log-to-disk?)
  (write-time :stream stream)
  (write-char #\Space stream)
  (case command
    (:privmsg
     (write-privmsg message user stream log-to-disk?))
    (:notice
     (write-notice message t user stream log-to-disk?))
    (:ping
     (write-ping message stream))
    (:join
     (when log-to-disk?
       (with-irc-message-type-1 (string (a parameter-index-array)) message
         (with-array-accessors* ((channel-start a :offset 0))
           (multiple-value-bind (filename-string filename)
               (make-log-path string channel-start)
             (let ((log-file (gethash filename-string (logs *log-files*))))
               (with-log-file (file-stream) log-file
                 (open-log-file-if-necessary log-file filename)
                 (write-join message file-stream)))))))
     (write-join message stream))
    (:part
     (when log-to-disk?
       (with-irc-message-type-2 (string (a parameter-index-array)) message
         (with-array-accessors* ((channel-start a :offset 0)
                                 (channel-end   a :offset 1))
           (format t "~A~%" (make-log-path string channel-start channel-end)))))
     (write-part message stream))
    (:kick
     (when log-to-disk?
       (with-irc-message-type-2 (string (a parameter-index-array)) message
         (with-array-accessors* ((channel-start a :offset 0)
                                 (channel-end   a :offset 1))
           (format t "~A~%" (make-log-path string channel-start channel-end)))))
     (write-kick message stream))
    ;; TODO: determine which files to log this in
    (:quit
     (write-quit message stream))
    ;; TODO: If this is the user changing nicks, use a different
    ;; message?
    ;;
    ;; TODO: When updating (client-nick user) on nick change (not
    ;; implemented yet), which nick is it going to have at this
    ;; point in the logging? The old one or the new one? Make
    ;; sure it doesn't get a 4XX in response.
    ;;
    ;; TODO: Should client-nick only update when the server sends
    ;; the message, in case a 433 (or other 4XX) rejects the
    ;; requested nick change? Also, the server might force a nick
    ;; change that wasn't requested.
    ;;
    ;; TODO: determine which files to log this in
    (:nick
     (write-nick message stream))
    (:mode
     (when log-to-disk?
       (with-irc-message-type-2 (string (a parameter-index-array)) message
         (with-array-accessors* ((target-start a :offset 0)
                                 (target-end   a :offset 1))
           (format t "~A~%" (make-log-path string target-start target-end)))))
     (write-mode message stream))
    (:topic
     (when log-to-disk?
       (with-irc-message-type-2 (string (a parameter-index-array)) message
         (with-array-accessors* ((channel-start a :offset 0)
                                 (channel-end   a :offset 1))
           (format t "~A~%" (make-log-path string channel-start channel-end)))))
     (write-topic message stream))
    (:invite
     (when log-to-disk?
       (with-irc-message-type-2 (string (a parameter-index-array)) message
         (with-array-accessors* ((channel-start a :offset 2))
           (unless (minusp channel-start)
             (format t "~A~%" (make-log-path string channel-start))))))
     (write-invite message user stream))
    (t
     (write-unknown-message message stream))))

;;; TODO: Where is KNOCK specified? log it!
;;;
;;; TODO: log WALLOPS
;;;
;;; TODO: optionally split output to different places based on
;;; destination (for logs and for clients); special case handle
;;; destinations like @#example instead of #example... and possibly
;;; even ChanServ join messages. When split into separate channels,
;;; don't show the destination for PRIVMSG and CTCP ACTION.
;;;
;;; TODO: QUITs are logged in every channel where the user has quit
;;; but not every channel of per-channel logs; NICK behaves similarly.
;;; This requires state tracking. The global logging, however, doesn't
;;; require state tracking.
;;;
;;; TODO:
;;;  - NAMES gives RPL_NAMREPLY (353) RPL_ENDOFNAMES (366); more useful with state
;;;  - WHOIS gives many, many replies, depending on the ircd (btwn lines 311 and 318)
;;;  - WHOWAS gives a subset of WHOIS as well as a few of its own (btwn lines 314 and 369)
;;;
;;; TODO: handle DCC
;;;
;;; TODO: Number the banlist in 367? Requires some prior (per-channel!) state.
(defun log-message (message user stream &key hide-ping? log-to-disk?)
  "Logs messages in a familiar human-readable format."
  (with-irc-message (command-symbol)
      message
    (unless (and hide-ping? (or (eql command-symbol :ping) (eql command-symbol :pong)))
      (typecase command-symbol
        (keyword
         (write-named-message command-symbol message user stream log-to-disk?))
        (number
         (write-irc-numeric command-symbol message user stream log-to-disk?))))))

(defun log-command (command parameters user log &key hide-ping? ignore-silent-commands?)
  (when (and hide-ping? (or (eql command :ping) (eql command :pong)))
    (return-from log-command))
  (when (and ignore-silent-commands?
             (keywordp command)
             (member command '(:join :part :nick :user :mode
                               :topic :invite :kick :names
                               :list :whois :whowas :who)))
    (return-from log-command))
  ;; TODO: log :QUIT; when supporting multiple servers/networks it
  ;; will have to track which one it goes to, and log as "disconnect"
  ;; rather than quit
  (case command
    ((:privmsg :notice)
     (let* ((source-start 1)
            (source-end (+ source-start (length (client-nick user))))
            (source-split source-end)
            (command-start (1+ source-end))
            (command-end (+ command-start (length (symbol-name command))))
            (parameters-start (1+ command-end))
            (string (format nil
                            ":~A ~A ~A"
                            (client-nick user)
                            (symbol-name command)
                            parameters))
            (param-1-start parameters-start)
            (param-1-end (next-separator-position string param-1-start))
            (param-2-start (and param-1-end
                                (if (char= #\: (aref string (1+ param-1-end)))
                                    (+ 2 param-1-end)
                                    (1+ param-1-end))))
            (ctcp? (and param-2-start
                        (char= (code-char #x01)
                               (aref string param-2-start))))
            (message (make-irc-message-type-2 :command-symbol command
                                              :source-start (or source-start -1)
                                              :source-split (or source-split -1)
                                              :source-end (or source-end -1)
                                              :command-start command-start
                                              :command-end command-end
                                              :parameters parameters-start
                                              :string string
                                              :ctcp? ctcp?
                                              :parameter-index-array (let ((a (make-parameter-index-array 3)))
                                                                       (psetf (aref a 0) param-1-start
                                                                              (aref a 1) param-1-end
                                                                              (aref a 2) param-2-start)
                                                                       a)))
            (log-to-disk? nil)
            (self-message? (string= (client-nick user)
                                    string
                                    :start2 param-1-start
                                    :end2 param-1-end)))
       (unless self-message?
         (write-time :stream log)
         (write-char #\Space log)
         (if (eql command :privmsg)
             (write-privmsg message user log log-to-disk?)
             (write-notice message parameters user log log-to-disk?)))))
    (:pong
     (write-time :stream log)
     (write-char #\Space log)
     (write-string "-!- [PONG] " log)
     (write-string parameters
                   log
                   :start (if (char= #\: (aref parameters 0)) 1 0))
     (terpri log))
    (t
     (write-time :stream log)
     (write-char #\Space log)
     (write-string (symbol-name command) log)
     (write-char #\Space log)
     (write-string parameters log)
     (terpri log))))
