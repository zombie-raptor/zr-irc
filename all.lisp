(uiop:define-package #:zr-irc/all
  (:nicknames #:zr-irc)
  (:use #:cl)
  (:use-reexport #:zr-irc/bot
                 #:zr-irc/commands
                 #:zr-irc/core
                 #:zr-irc/irc
                 #:zr-irc/log
                 #:zr-irc/log-file
                 #:zr-irc/util))
