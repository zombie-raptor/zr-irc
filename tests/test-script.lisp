(defpackage #:test-script
  (:use #:cl))

(in-package #:test-script)

;;; Load the test suite and UIOP at read time.
#.(progn (ql:quickload :fiveam :silent t)
         (ql:quickload :uiop :silent t)
         nil)

;;; Turn on full type inference to catch more type errors when
;;; compiling.
#+sbcl
(setf sb-ext:*derive-function-types* t)

;;; Register the systems in the same way as in the project's ASDF file
;;; because not every dependency's ASDF system lines up with its QL
;;; project name. This might be unnecessary.
(dolist (system-packages '((:fiveam      (:it.bese.fiveam))))
  (destructuring-bind (system packages)
      system-packages
    (asdf:register-system-packages system packages)))

;;; Load everything except for the main system so that the main system
;;; can error on warnings, but so the warnings in the dependencies
;;; (which exist!) don't error. Don't load verbosely because this is
;;; just noise for the logs.
(dolist (system (asdf:system-depends-on (asdf:find-system :zr-irc)))
  (when (string/= system "zr-irc/all")
    (ql:quickload system)))

;;; Load only the parts required by the test system. Load with verbose
;;; so every message is visible. Error on warnings so the CI fails.
(let ((asdf:*compile-file-warnings-behaviour* :error))
  (ql:quickload :zr-irc/tests :verbose t))

;;; Run the tests.
;;;
;;; Ideally, we could just work with the final return value of the
;;; fiveam tests from asdf:test-system, but asdf:test-system always
;;; returns t, so fiveam has to be used directly.
(defun run-tests ()
  (fiveam:run! 'zr-irc/tests:zr-irc/tests))

;;; This lets Gitlab CI know that something went wrong.
(unless (run-tests)
  (uiop:quit 1))
