(cl:defpackage #:zr-irc/tests/all
  (:use #:cl #:zr-utils)
  (:import-from #:fiveam
                #:def-suite
                #:for-all
                #:in-suite
                #:is
                #:test)
  (:import-from #:zr-irc/log
                #:log-message)
  (:import-from #:zr-irc/irc
                #:make-client
                #:parse-irc-message)
  (:export #:irc-log
           #:log-to-string
           #:log-to-string*
           #:zr-irc/tests))

(cl:in-package #:zr-irc/tests/all)

(def-suite zr-irc/tests)

(in-suite zr-irc/tests)

(defun log-to-string (message user)
  "Logs an IRC message to a string."
  (with-output-to-string (log)
    (log-message message user log)))

(defun log-to-string* (message user)
  "
Logs an IRC message to a string, excluding the timestamp and the
trailing newline, for easy unit testing.
"
  (let ((logged-message (log-to-string message user)))
    (subseq logged-message
            (length "00:00:00 ")
            (- (length logged-message) 1))))

(defun irc-log (message client)
  "
Parses an IRC message into the internal format, and then logs it.
"
  (log-to-string* (parse-irc-message message) client))
