(cl:defpackage #:zr-irc/tests/log
  (:use #:cl
        #:zr-utils
        #:zr-irc/tests/all)
  (:import-from #:fiveam
                #:def-suite
                #:for-all
                #:in-suite
                #:is
                #:test)
  (:import-from #:zr-irc/irc
                #:make-client)
  (:export #:zr-irc/tests/log/syntax))

(cl:in-package #:zr-irc/tests/log)

(def-suite zr-irc/tests/log/syntax
  :in zr-irc/tests)

(in-suite zr-irc/tests/log/syntax)

(test privmsg
  (let ((example-client (make-client :nick "example")))
    (is (string= "<source:#target> This is a message."
                 (irc-log ":source!name@host PRIVMSG #target :This is a message." example-client)))
    (is (string= "<source:#target> This message is incorrect, but still is accepted."
                 (irc-log ":source!name@host PRIVMSG #target This message is incorrect, but still is accepted." example-client)))
    (is (string= "<source:#target> text"
                 (irc-log ":source!name@host PRIVMSG #target :text" example-client)))
    (is (string= "<source:@#target> text"
                 (irc-log ":source PRIVMSG @#target :text" example-client)))
    (is (string= "<source:&target> text"
                 (irc-log ":source PRIVMSG &target :text" example-client)))
    (is (string= "<source:#target> still-valid-text"
                 (irc-log ":source PRIVMSG #target still-valid-text" example-client)))
    (is (string= "<source:example> text"
                 (irc-log ":source!name@host PRIVMSG example :text" example-client)))
    (is (string= "<source:example> text"
                 (irc-log ":source PRIVMSG example :text" example-client)))))

;;; Note: Your editor might not display the unprintable #x01 character
;;; that is either on either side of the CTCP message or only between
;;; the : and the CTCP command name, depending on the particular test.
(test privmsg/ctcp/action
  (let ((example-client (make-client :nick "example")))
    (is (string= "* source:#target is sending an action"
                 (irc-log ":source!name@host PRIVMSG #target :ACTION is sending an action" example-client)))
    (is (string= "* source:example is sending an action"
                 (irc-log ":source!name@host PRIVMSG example :ACTION is sending an action" example-client)))
    (is (string= "* source:example is sending a slightly incorrect message."
                 (irc-log ":source!name@host PRIVMSG example ACTION is sending a slightly incorrect message." example-client)))
    (is (string= "* source:#target acts"
                 (irc-log ":source!name@host PRIVMSG #target :ACTION acts" example-client)))
    (is (string= "* source:#target acts"
                 (irc-log ":source!name@host PRIVMSG #target :ACTION acts" example-client)))
    (is (string= "* source:#target "
                 (irc-log ":source!name@host PRIVMSG #target :ACTION " example-client)))
    (is (string= "* source:#target "
                 (irc-log ":source!name@host PRIVMSG #target :ACTION " example-client)))))

(test ctcp
  (let ((example-client (make-client :nick "example")))
    (is (string= "[->CTCP source->destination VERSION]"
                 (irc-log ":source!name@host PRIVMSG destination :VERSION " example-client)))
    (is (string= "[->CTCP source->destination VERSION]"
                 (irc-log ":source!name@host PRIVMSG destination :VERSION " example-client)))
    (is (string= "[->CTCP source->destination VERSION]"
                 (irc-log ":source!name@host PRIVMSG destination :VERSION" example-client)))
    (is (string= "[->CTCP source->destination VERSION]"
                 (irc-log ":source!name@host PRIVMSG destination :VERSION" example-client)))
    (is (string= "[->CTCP source->destination PING] 1234 5678"
                 (irc-log ":source PRIVMSG destination :PING 1234 5678" example-client)))
    (is (string= "[->CTCP source->destination PING] 1234 5678"
                 (irc-log ":source PRIVMSG destination :PING 1234 5678" example-client)))))

(test nctcp
  (let ((example-client (make-client :nick "example")))
    (is (string= "[<-CTCP source->destination VERSION]"
                 (irc-log ":source!name@host NOTICE destination :VERSION " example-client)))
    (is (string= "[<-CTCP source->destination VERSION]"
                 (irc-log ":source!name@host NOTICE destination :VERSION " example-client)))
    (is (string= "[<-CTCP source->destination VERSION]"
                 (irc-log ":source!name@host NOTICE destination :VERSION" example-client)))
    (is (string= "[<-CTCP source->destination VERSION]"
                 (irc-log ":source!name@host NOTICE destination :VERSION" example-client)))
    (is (string= "[<-CTCP source->destination PING] 1234 5678"
                 (irc-log ":source NOTICE destination :PING 1234 5678" example-client)))
    (is (string= "[<-CTCP source->destination PING] 1234 5678"
                 (irc-log ":source NOTICE destination :PING 1234 5678" example-client)))))

(test notice
  (let ((example-client (make-client :nick "example")))
    (is (string= "-source:#target- This is a message."
                 (irc-log ":source!name@host NOTICE #target :This is a message." example-client)))
    (is (string= "-source:#target- This message is incorrect, but still is accepted."
                 (irc-log ":source!name@host NOTICE #target This message is incorrect, but still is accepted." example-client)))
    (is (string= "-source:#target- text"
                 (irc-log ":source!name@host NOTICE #target :text" example-client)))
    (is (string= "-source:&target- text"
                 (irc-log ":source NOTICE &target :text" example-client)))
    (is (string= "-source:#target- still-valid-text"
                 (irc-log ":source NOTICE #target still-valid-text" example-client)))
    (is (string= "-source(name@host)- text"
                 (irc-log ":source!name@host NOTICE example :text" example-client)))
    (is (string= "!source text"
                 (irc-log ":source NOTICE example :text" example-client)))))

(test ping
  (let ((example-client (make-client :nick "example")))
    (is (string= "-!- [PING] source-and-dest"
                 (irc-log "PING :source-and-dest" example-client)))
    (is (string= "-!- [PING] source-and-dest"
                 (irc-log "PING source-and-dest" example-client)))
    (is (string= "-!- [PING] source dest"
                 (irc-log "PING source :dest" example-client)))
    (is (string= "-!- [PING] source dest"
                 (irc-log "PING source dest" example-client)))))

(test join
  (let ((example-client (make-client :nick "example")))
    (is (string= "--> nick [name@host] has joined #channel"
                 (irc-log ":nick!name@host JOIN :#channel" example-client)))
    (is (string= "--> nick [name@host] has joined #channel"
                 (irc-log ":nick!name@host JOIN #channel" example-client)))
    (is (string= "--> nick [] has joined #channel"
                 (irc-log ":nick JOIN :#channel" example-client)))
    (is (string= "--> nick [] has joined #channel"
                 (irc-log ":nick JOIN #channel" example-client)))))

(test part
  (let ((example-client (make-client :nick "example")))
    (is (string= "<-- nick [name@host] has left #channel []"
                 (irc-log ":nick!name@host PART #channel" example-client)))
    (is (string= "<-- nick [name@host] has left #channel []"
                 (irc-log ":nick!name@host PART :#channel" example-client)))
    (is (string= "<-- nick [name@host] has left #channel [This is a reason.]"
                 (irc-log ":nick!name@host PART #channel :This is a reason." example-client)))
    (is (string= "<-- nick [name@host] has left #channel [This is a reason.]"
                 (irc-log ":nick!name@host PART #channel This is a reason." example-client)))
    (is (string= "<-- nick [name@host] has left #channel [Reason.]"
                 (irc-log ":nick!name@host PART #channel Reason." example-client)))
    (is (string= "<-- nick [] has left #channel []"
                 (irc-log ":nick PART #channel" example-client)))
    (is (string= "<-- nick [] has left #channel []"
                 (irc-log ":nick PART :#channel" example-client)))
    (is (string= "<-- nick [] has left #channel [This is a reason.]"
                 (irc-log ":nick PART #channel :This is a reason." example-client)))
    (is (string= "<-- nick [] has left #channel [This is a reason.]"
                 (irc-log ":nick PART #channel This is a reason." example-client)))
    (is (string= "<-- nick [] has left #channel [Reason.]"
                 (irc-log ":nick PART #channel Reason." example-client)))))

(test kick
  (let ((example-client (make-client :nick "example")))
    (is (string= "<-- nick was kicked from #channel by chan-op [This is a reason.]"
                 (irc-log ":chan-op!op-name@op-host KICK #channel nick :This is a reason." example-client)))
    (is (string= "<-- nick was kicked from #channel by chan-op [This is a reason.]"
                 (irc-log ":chan-op KICK #channel nick :This is a reason." example-client)))
    (is (string= "<-- nick was kicked from #channel by chan-op [Reason]"
                 (irc-log ":chan-op KICK #channel nick Reason" example-client)))
    (is (string= "<-- nick was kicked from #channel by chan-op [This is slightly invalid, but it still works.]"
                 (irc-log ":chan-op KICK #channel nick This is slightly invalid, but it still works." example-client)))
    (is (string= "<-- nick was kicked from #channel by chan-op []"
                 (irc-log ":chan-op KICK #channel nick" example-client)))
    (is (string= "<-- nick was kicked from #channel by chan-op []"
                 (irc-log ":chan-op KICK #channel nick " example-client)))
    (is (string= "<-- nick was kicked from #channel by chan-op []"
                 (irc-log ":chan-op KICK #channel nick :" example-client)))
    (is (string= "-!- Invalid KICK message: no target nick provided"
                 (irc-log ":nick KICK #channel" example-client)))))

(test quit
  (let ((example-client (make-client :nick "example")))
    (is (string= "<-- nick [name@host] has quit [Quit: This is a reason.]"
                 (irc-log ":nick!name@host QUIT :Quit: This is a reason." example-client)))
    (is (string= "<-- nick [name@host] has quit [This is a reason.]"
                 (irc-log ":nick!name@host QUIT :This is a reason." example-client)))
    (is (string= "<-- nick [name@host] has quit [Quit: This is a reason.]"
                 (irc-log ":nick!name@host QUIT Quit: This is a reason." example-client)))
    (is (string= "<-- nick [name@host] has quit [This is a reason.]"
                 (irc-log ":nick!name@host QUIT This is a reason." example-client)))
    (is (string= "<-- nick [name@host] has quit [reason]"
                 (irc-log ":nick!name@host QUIT reason" example-client)))
    (is (string= "<-- nick [] has quit [Quit: This is a reason.]"
                 (irc-log ":nick QUIT :Quit: This is a reason." example-client)))
    (is (string= "<-- nick [] has quit [This is a reason.]"
                 (irc-log ":nick QUIT :This is a reason." example-client)))
    (is (string= "<-- nick [] has quit [Quit: This is a reason.]"
                 (irc-log ":nick QUIT Quit: This is a reason." example-client)))
    (is (string= "<-- nick [] has quit [This is a reason.]"
                 (irc-log ":nick QUIT This is a reason." example-client)))
    (is (string= "<-- nick [] has quit [reason]"
                 (irc-log ":nick QUIT reason" example-client)))))

(test nick
  (let ((example-client (make-client :nick "example")))
    (is (string= "-!- nick is now known as new"
                 (irc-log ":nick!@name@host NICK new" example-client)))
    (is (string= "-!- nick is now known as new"
                 (irc-log ":nick NICK new" example-client)))
    (is (string= "-!- nick is now known as new"
                 (irc-log ":nick!@name@host NICK :new" example-client)))
    (is (string= "-!- nick is now known as new"
                 (irc-log ":nick NICK :new" example-client)))))

(test mode/channel
  (let ((example-client (make-client :nick "example")))
    (is (string= "-!- mode/#channel [+nt] by irc.example.com"
                 (irc-log ":irc.example.com MODE #channel +nt " example-client)))
    (is (string= "-!- mode/#channel [+nt] by irc.example.com"
                 (irc-log ":irc.example.com MODE #channel +nt" example-client)))
    (is (string= "-!- mode/#channel [+b nick!name@host] by chan-op"
                 (irc-log ":chan-op!op-name@op-host MODE #channel +b nick!name@host" example-client)))
    (is (string= "-!- mode/#channel [+b nick!*@*] by chan-op"
                 (irc-log ":chan-op MODE #channel +b nick!*@*" example-client)))
    (is (string= "-!- mode/#channel [+o-v new-op new-op] by existing-op"
                 (irc-log ":existing-op!user@example MODE #channel +o-v new-op new-op" example-client)))))

;;; TODO: more tests
(test mode/user
  (let ((example-client (make-client :nick "example")))
    (is (string= "-!- mode/nickname [+R] by nickname"
                 (irc-log ":nickname!foo@example MODE nickname +R" example-client)))
    (is (string= "-!- mode/nickname [+R] by nickname"
                 (irc-log ":nickname MODE nickname +R" example-client)))
    (is (string= "-!- mode/nickname [+R] by nickname"
                 (irc-log ":nickname!foo@example MODE nickname :+R" example-client)))
    (is (string= "-!- mode/nickname [+R] by nickname"
                 (irc-log ":nickname MODE nickname :+R" example-client)))))

(test topic
  (let ((example-client (make-client :nick "example")))
    (is (string= "-!- op set the topic of #channel to: Hello world!"
                 (irc-log ":op TOPIC #channel :Hello world!" example-client)))
    (is (string= "-!- op set the topic of #channel to: Hello world!"
                 (irc-log ":op!name@host TOPIC #channel :Hello world!" example-client)))
    (is (string= "-!- op set the topic of #channel to: Topic"
                 (irc-log ":op TOPIC #channel Topic" example-client)))
    (is (string= "-!- op set the topic of #channel to: Invalid syntax, but still parses."
                 (irc-log ":op TOPIC #channel Invalid syntax, but still parses." example-client)))))

(test invite
  (let ((example-client (make-client :nick "example")))
    (is (string= "-!- op invites you to #channel"
                 (irc-log ":op!name@host INVITE example #channel" example-client)))
    (is (string= "-!- op invites you to #channel"
                 (irc-log ":op!name@host INVITE example :#channel" example-client)))
    (is (string= "-!- op invites you to #channel"
                 (irc-log ":op INVITE example #channel" example-client)))
    (is (string= "-!- op invites you to an unknown channel (invalid INVITE message)"
                 (irc-log ":op INVITE example" example-client)))
    (is (string= "-!- op invites someone to #channel"
                 (irc-log ":op!name@host INVITE someone #channel" example-client)))
    (is (string= "-!- op invites someone to #channel"
                 (irc-log ":op!name@host INVITE someone :#channel" example-client)))
    (is (string= "-!- op invites someone to #channel"
                 (irc-log ":op INVITE someone #channel" example-client)))))

;;; TODO: more tests:
;;;
;;; TODO: numerics that are not printed specially
;;; TODO: numerics that are printed specially (sometimes statefully)

;;; TODO: test logging commands that need to be logged because the
;;; meaningful state is the part that is sent rather than the part
;;; that the server sends back (e.g. PRIVMSG and NOTICE)

;;; TODO: test anything that doesn't have tests in log-message or
;;; log-command
