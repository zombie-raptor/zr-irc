(cl:defpackage #:zr-irc/irc
  (:use #:cl
        #:zr-utils
        #:zr-irc/commands
        #:zr-irc/core
        #:zr-irc/log
        #:zr-irc/log-file
        #:zr-irc/util)
  (:import-from #:bordeaux-threads)
  (:import-from #:zr-irc/bot
                #:auto-reply)
  (:export #:simple-background-connection
           #:simple-connection))

(cl:in-package #:zr-irc/irc)

;;; TODO: Optionally log the original, unprocessed form of the
;;; incoming and/or outgoing messages

(defun init (user connection log)
  (with-client (nick username real-name) user
    (write-command :nick nick connection log user)
    (write-command :user (format nil "~A 0 * ~A" username real-name) connection log user)
    (force-output connection)))

(define-function (parse-irc-message :return ((maybe irc-message))) ((message-string string))
  (when (zerop (length message-string))
    (warn "Blank IRC command received.")
    (return-from parse-irc-message))
  (let* ((source-start (if (char= #\: (aref message-string 0)) 1 nil))
         (source-end (if source-start (next-separator-position message-string)))
         ;; Splits at the ! if there is one.
         (source-split (and source-start
                            (or (position #\! message-string
                                          :start source-start
                                          :end source-end)
                                source-end)))
         (command-start (if source-end
                            (skip-extra-spaces message-string source-end)
                            0))
         (command-end (next-separator-position message-string command-start))
         (command-symbol (if (digit-char-p (aref message-string command-start))
                             (parse-integer message-string
                                            :start command-start
                                            :end command-end)
                             (intern (subseq message-string
                                             command-start
                                             command-end)
                                     '#:keyword)))
         (parameters (skip-extra-spaces message-string command-end)))
    (case command-symbol
      ((:join :quit :nick)
       (make-irc-message-type-1 :command-symbol command-symbol
                                :source-start (or source-start -1)
                                :source-split (or source-split -1)
                                :source-end (or source-end -1)
                                :command-start command-start
                                :command-end command-end
                                :parameters parameters
                                :string message-string
                                :ctcp? nil
                                :parameter-index-array (make-parameter-index-array-type-1 message-string parameters)))
      ((:privmsg :notice :ping :part :mode :topic :invite)
       (let ((a (make-parameter-index-array-type-2 message-string parameters)))
         (let ((ctcp? (and (or (eql command-symbol :privmsg)
                               (eql command-symbol :notice))
                           (and (not (minusp (aref a 2)))
                                (< (aref a 2) (length message-string))
                                (char= (code-char #x01)
                                       (aref message-string (aref a 2)))))))
           (make-irc-message-type-2 :command-symbol command-symbol
                                    :source-start (or source-start -1)
                                    :source-split (or source-split -1)
                                    :source-end (or source-end -1)
                                    :command-start command-start
                                    :command-end command-end
                                    :parameters parameters
                                    :string message-string
                                    :ctcp? ctcp?
                                    :parameter-index-array a))))
      (:kick
       (make-irc-message-type-3 :command-symbol command-symbol
                                :source-start (or source-start -1)
                                :source-split (or source-split -1)
                                :source-end (or source-end -1)
                                :command-start command-start
                                :command-end command-end
                                :parameters parameters
                                :string message-string
                                :ctcp? nil
                                :parameter-index-array (make-parameter-index-array-type-3 message-string parameters)))
      (t
       (make-irc-message :command-symbol command-symbol
                         :source-start (or source-start -1)
                         :source-split (or source-split -1)
                         :source-end (or source-end -1)
                         :command-start command-start
                         :command-end command-end
                         :parameters parameters
                         :string message-string
                         :ctcp? nil)))))

(define-function handle-ctcp ((irc-message irc-message)
                              (user client)
                              (log stream)
                              trail)
  (declare (ignorable log))
  (with-irc-message (source-start source-end string) irc-message
    (with-client (nick real-name version user-info) user
      (let ((command-end (or (next-separator-position string (1+ trail))
                             (position (code-char #x01) string :start (1+ trail))))
            (source (subseq string
                            source-start
                            (or (position #\! string :start source-start :end source-end)
                                source-end))))
        (cond ((string-equal "PING" string :start2 (1+ trail) :end2 command-end)
               (nctcp source
                      "PING"
                      (subseq string
                              (+ trail (length "PING") 2)
                              (position (code-char #x01)
                                        string
                                        :start (+ trail (length "PING") 2)))))
              ((string-equal "VERSION" string :start2 (1+ trail) :end2 command-end)
               (nctcp source "VERSION" (or version "ZR IRC 0")))
              ((string-equal "TIME" string :start2 (1+ trail) :end2 command-end)
               (let ((time (with-output-to-string (output)
                             (write-datetime :utc? t :stream output))))
                 (nctcp source "TIME" time)))
              ((string-equal "USERINFO" string :start2 (1+ trail) :end2 command-end)
               (nctcp source
                      "USERINFO"
                      (or user-info
                          (format nil "~A (~A)" nick real-name))))
              ((string-equal "CLIENTINFO" string :start2 (1+ trail) :end2 command-end)
               (nctcp source "CLIENTINFO" "PING VERSION TIME USERINFO CLIENTINFO")))))))

(defun handle-command (irc-message user stream &key auto-reply (log *standard-output*))
  "
First, this logs incoming IRC messages based on their type. Next, this
handles any commands from those message that must be handled (PING) or
any that can be handled (mostly CTCPs). Finally, bot-like
functionality can be handled if needed.
"
  (log-message irc-message user log :hide-ping? t ; :log-to-disk? t
               )
  (with-irc-message (command-symbol source-start source-split source-end parameters string ctcp?)
      irc-message
    (when (keywordp command-symbol)
      (case command-symbol
        ;; Note: This is a direct response instead of being added to
        ;; the command queue. The PING must receive a PONG response as
        ;; soon as possible. Everything else can be delayed.
        (:ping
         (multiple-value-bind (source-start source-end destination-start)
             (two-parameter-message string parameters)
           (if destination-start
               (write-command :pong (subseq string destination-start) stream log user)
               (write-command :pong (subseq string source-start source-end) stream log user)))
         (force-output stream))
        ;; Only respond to PRIVMSG that have a source and a
        ;; well-formed message parameter trail.
        (:privmsg
         (unless (or (minusp source-start) (minusp source-end))
           (multiple-value-bind (target-start target-end message-start)
               (two-parameter-message string parameters)
             (if ctcp?
                 (handle-ctcp irc-message user log message-start)
                 (when auto-reply
                   (let* ((speaker (subseq string source-start source-split)))
                     (when (message-prefix-match? (client-nick user)
                                                  string
                                                  :suffix-char #\:
                                                  :message-start message-start)
                       (msg (if (message-prefix-match? (client-nick user)
                                                       string
                                                       :message-start message-start
                                                       :suffix-char #\Space)
                                speaker
                                (subseq string target-start target-end))
                            (funcall auto-reply speaker)))))))))))))

(defun write-connection-attempt-message (server log)
  (write-time :stream log)
  (format log " Attempting connection to address ")
  (write-ip (server-address server) :log log)
  (format log " port ~A~%" (server-port server)))

(defun write-connection-message (connection log)
  (multiple-value-bind (ip port) (usocket:get-peer-name connection)
    (write-time :stream log)
    (write-string " Connecting to " log)
    (write-ip ip :port port :log log)
    (terpri log)))

(define-function (read-irc-message :return (simple-string boolean))
    ((octets (simple-array octet (512))) connection log (start bit))
  (declare (ignore log))
  (multiple-value-bind (length eof?)
      (loop :for byte :of-type (maybe octet) := (read-byte connection nil)
            :for i :from start :below 512
            :while (and byte (/= byte #x0A))
            :do (setf (aref octets i) byte)
            :finally (return (values i (not byte))))
    (when (and (not eof?) (/= #x0D (aref octets (1- length))))
      (warn "IRC message ended in \\n instead of \\r\\n"))
    (values (sb-ext:octets-to-string octets :end (max (1- length) 0))
            eof?)))

(defun irc-loop (user server stream connection log)
  (declare (ignore server))
  (let ((quit? nil)
        (connection-lost? nil)
        (last-message (float-time)))
    (unwind-protect
         (let ((octets (make-array 512 :element-type 'octet)))
           (labels ((read-irc-message* (&optional (start 0))
                      (multiple-value-bind (message eof?)
                          (read-irc-message octets stream log start)
                        (if eof?
                            (prog1 nil
                              (setf connection-lost? t)
                              (write-time :stream log)
                              (write-line " Connection lost." log))
                            (progn
                              (setf last-message (float-time))
                              (parse-irc-message message)))))
                    ;; Every 30 seconds of no network input (including
                    ;; pings), a blocking read inside of a timeout has
                    ;; to be done. This checks to see if the TCP
                    ;; connection is still alive (i.e. hanging on
                    ;; input instead of reading an EOF). This is
                    ;; because LISTEN is NIL on EOF and there is no
                    ;; READ-BYTE-NO-HANG function.
                    ;;
                    ;; In theory, a timeout abort at just the right
                    ;; time might get this to miss a PING and ping
                    ;; out, but hopefully it doesn't happen in
                    ;; practice.
                    (eof-test ()
                      (let ((possible-eof (handler-case
                                              (bt:with-timeout (0.1f0)
                                                (read-byte stream nil :eof))
                                            (bt:timeout () nil))))
                        (cond ((eql :eof possible-eof)
                               (setf connection-lost? t)
                               (write-time :stream log)
                               (write-line " Connection lost." log)
                               nil)
                              (possible-eof
                               (setf (aref octets 0) possible-eof)
                               (read-irc-message* 1))
                              (t nil)))))
             (declare (inline read-irc-message* eof-test))
             (ensure-log-directory-exists #P"zr-irc-logs/")
             (init user stream log)
             (loop :until (or quit? connection-lost?)
                   :for irc-message :of-type (maybe irc-message)
                     := (cond ((listen stream)
                               (read-irc-message*))
                              ((> (- (float-time) last-message) 30f0)
                               (eof-test))
                              (t nil))
                   :when irc-message
                     :do (handle-command irc-message user stream :auto-reply #'auto-reply :log log)
                   :unless (command-queue-empty?)
                     :do (setf quit? (run-commands-from-queue user stream log))
                   :do (progn
                         ;; Close the inactive log files about once
                         ;; every 10 seconds so files for inactive
                         ;; channels/queries aren't kept open for too
                         ;; long.
                         (when (< (mod (float-time) 10) 0.015f0)
                           (close-inactive-logs))
                         (sleep 0.01f0)))))
      (when (and (open-stream-p stream) (not (or quit? connection-lost?)))
        (write-command :quit "" stream log user))
      (usocket:socket-close connection)
      (cleanup *log-files*))))

(defun simple-connection (nick
                          &key
                            (name "Localhost")
                            (internal-name :localhost)
                            (address "localhost")
                            (port 6667)
                            (output-stream *standard-output*))
  (let* ((server (make-server :name name
                              :internal-name internal-name
                              :address address
                              :nick nick
                              :port port))
         (user (make-client :nick nick
                            :real-name "Example"
                            :username "example"
                            :version "Example 1.0"
                            :user-info "Example!"))
         (connection (progn
                       (write-connection-attempt-message server output-stream)
                       (usocket:socket-connect (server-address server)
                                               (server-port server)
                                               :element-type 'octet)))
         (stream (usocket:socket-stream connection)))
    (write-connection-message connection output-stream)
    (irc-loop user server stream connection output-stream)
    nil))

(defun simple-background-connection (nick
                                     &key
                                       (name "Localhost")
                                       (internal-name :localhost)
                                       (address "localhost")
                                       (port 6667)
                                       (output-stream *standard-output*))
  (bt:make-thread (lambda ()
                    (simple-connection nick
                                       :name name
                                       :internal-name internal-name
                                       :address address
                                       :port port
                                       :output-stream output-stream))))
