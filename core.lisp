(cl:defpackage #:zr-irc/core
  (:use #:cl
        #:zr-utils
        #:zr-irc/util)
  (:export #:client
           #:client-nick
           #:client-real-name
           #:client-user-info
           #:client-username
           #:client-version
           #:four-parameter-message
           #:irc-message
           #:irc-message-type-1
           #:irc-message-type-2
           #:irc-message-type-3
           #:irc-message-type-4
           #:make-client
           #:make-irc-message
           #:make-irc-message-type-1
           #:make-irc-message-type-2
           #:make-irc-message-type-3
           #:make-irc-message-type-4
           #:make-parameter-index-array
           #:make-parameter-index-array-type-1
           #:make-parameter-index-array-type-2
           #:make-parameter-index-array-type-3
           #:make-parameter-index-array-type-4
           #:make-server
           #:next-parameter-start
           #:one-parameter-message
           #:server
           #:server-address
           #:server-port
           #:three-parameter-message
           #:two-parameter-message
           #:with-client
           #:with-irc-message
           #:with-irc-message-type-1
           #:with-irc-message-type-2
           #:with-irc-message-type-3
           #:with-irc-message-type-4))

(cl:in-package #:zr-irc/core)

(defstruct server
  (name          nil  :type string)
  (internal-name nil  :type keyword)
  (nick          nil  :type (maybe string))
  (password      nil  :type (maybe string))
  (address       nil  :type string)
  (port          6667 :type uint16)
  (sasl          nil  :type boolean)
  (tcl           nil  :type boolean))

(defstruct client
  (nick      nil  :type string)
  (real-name "ZR" :type string)
  (username  "zr" :type string)
  ;; If nil, uses the default version string.
  (version   nil  :type (maybe string))
  ;; If nil, then uses "<nickname> (<realname>)"
  (user-info nil  :type (maybe string)))

(define-accessor-macro with-client #:client-)

#+(or)
(make-server :name "Libera Chat"
             :internal-name :libera
             :address "irc.libera.chat"
             :nick nick
             :port 6667)

(defstruct irc-message
  (command-symbol nil :type (or keyword uint16))
  (source-start   nil :type int16)
  (source-split   nil :type int16)
  (source-end     nil :type int16)
  (command-start  nil :type uint16)
  (command-end    nil :type uint16)
  ;; TODO: remove
  (parameters     nil :type uint16)
  (string         nil :type string)
  (ctcp?          nil :type boolean))

;;; Note: These could be replaced with arrays if NIL is turned into -1
;;; (and int16 instead of uint16) because specialized arrays must be
;;; homogeneous.
(defstruct (irc-message-type-1 (:include irc-message))
  (parameter-index-array nil :type (simple-array int16 (1))))

(defstruct (irc-message-type-2 (:include irc-message))
  (parameter-index-array nil :type (simple-array int16 (3))))

(defstruct (irc-message-type-3 (:include irc-message))
  (parameter-index-array nil :type (simple-array int16 (5))))

(defstruct (irc-message-type-4 (:include irc-message))
  (parameter-index-array nil :type (simple-array int16 (7))))

(define-accessor-macro with-irc-message #:irc-message-)
(define-accessor-macro with-irc-message-type-1 #:irc-message-type-1-)
(define-accessor-macro with-irc-message-type-2 #:irc-message-type-2-)
(define-accessor-macro with-irc-message-type-3 #:irc-message-type-3-)
(define-accessor-macro with-irc-message-type-4 #:irc-message-type-4-)

(define-function (make-parameter-index-array :inline t) (length)
  (make-array length :element-type 'int16 :initial-element -1))

(define-function (next-parameter-start :inline t) ((message string) start)
  "
Finds the start of the next parameter from the START index in MESSAGE.
If it starts with the #\: character, then the true start is one
character later. Starting with the #\: character also means that this
must be the last parameter; thus, the second return value must be T in
this case and NIL in all other situations.
"
  (and start
       (not (minusp start))
       (let ((next (skip-extra-spaces message start)))
         (if (and next (char= #\: (aref message next)))
             (values (1+ next) t)
             (values next nil)))))

(define-function (one-parameter-message :inline t) ((message string) parameters-start)
  (next-parameter-start message parameters-start))

(define-function (two-parameter-message :inline t) ((message string) parameters-start)
  (if (char= #\: (aref message parameters-start))
      (values (1+ parameters-start) nil nil)
      (let* ((first-end (next-separator-position message parameters-start))
             (second-start (next-parameter-start message first-end)))
        (values parameters-start
                first-end
                second-start))))

(define-function (three-parameter-message :inline t) ((message string) parameters-start)
  (if (char= #\: (aref message parameters-start))
      (values (1+ parameters-start) nil nil nil nil)
      (let ((first-end (next-separator-position message parameters-start)))
        (multiple-value-bind (second-start trail?)
            (next-parameter-start message first-end)
          (if trail?
              (values parameters-start first-end second-start nil nil)
              (let* ((second-end (next-separator-position message second-start))
                     (third-start (next-parameter-start message second-end)))
                (values parameters-start first-end second-start second-end third-start)))))))

(define-function (four-parameter-message :inline t) ((message string) parameters-start)
  (if (char= #\: (aref message parameters-start))
      (values (1+ parameters-start) nil nil nil nil nil nil)
      (let ((first-end (next-separator-position message parameters-start)))
        (multiple-value-bind (second-start trail?)
            (next-parameter-start message first-end)
          (if trail?
              (values parameters-start first-end second-start nil nil nil nil)
              (let ((second-end (next-separator-position message second-start)))
                (multiple-value-bind (third-start trail?)
                    (next-parameter-start message second-end)
                  (if trail?
                      (values parameters-start first-end second-start second-end third-start nil nil)
                      (let* ((third-end (next-separator-position message third-start))
                             (fourth-start (next-parameter-start message third-end)))
                        (values parameters-start first-end second-start second-end third-start third-end fourth-start))))))))))

(define-function (make-parameter-index-array-type-1 :inline t) (message-string start)
  (let ((a (make-parameter-index-array 1)))
    (multiple-value-bind (start1)
        (one-parameter-message message-string start)
      (psetf (aref a 0) (or start1 -1))
      a)))

(define-function (make-parameter-index-array-type-2 :inline t) (message-string start)
  (let ((a (make-parameter-index-array 3)))
    (multiple-value-bind (start1 end1 start2)
        (two-parameter-message message-string start)
      (psetf (aref a 0) (or start1 -1)
             (aref a 1) (or end1 -1)
             (aref a 2) (or start2 -1))
      a)))

(define-function (make-parameter-index-array-type-3 :inline t) (message-string start)
  (let ((a (make-parameter-index-array 5)))
    (multiple-value-bind (start1 end1 start2 end2 start3)
        (three-parameter-message message-string start)
      (psetf (aref a 0) (or start1 -1)
             (aref a 1) (or end1 -1)
             (aref a 2) (or start2 -1)
             (aref a 3) (or end2 -1)
             (aref a 4) (or start3 -1))
      a)))

(define-function (make-parameter-index-array-type-4 :inline t) (message-string start)
  (let ((a (make-parameter-index-array 7)))
    (multiple-value-bind (start1 end1 start2 end2 start3 end3 start4)
        (four-parameter-message message-string start)
      (psetf (aref a 0) (or start1 -1)
             (aref a 1) (or end1 -1)
             (aref a 2) (or start2 -1)
             (aref a 3) (or end2 -1)
             (aref a 4) (or start3 -1)
             (aref a 5) (or end3 -1)
             (aref a 6) (or start4 -1))
      a)))
