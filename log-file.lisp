(cl:defpackage #:zr-irc/log-file
  (:documentation "Manages creating, opening, closing, etc., log files.")
  (:use #:cl
        #:zr-utils)
  (:import-from #:uiop
                #:ensure-all-directories-exist
                #:subpathname)
  (:export #:*log-files*
           #:close-inactive-logs
           #:close-log-file-if-inactive
           #:create-log-if-necessary
           #:ensure-log-directory-exists
           #:log-file
           #:log-file-file-stream
           #:log-file-p
           #:log-file-time
           #:logs
           #:make-log-file
           #:open-log-file-if-necessary
           #:update-log-file-time
           #:with-log-file))

(cl:in-package #:zr-irc/log-file)

;;; Abstracts over the log files data structure, which stores all log
;;; files.
(defstruct (log-files (:conc-name nil))
  (logs (make-hash-table :test 'equal) :type hash-table))

(declaim (type log-files *log-files*))
(defvar *log-files* (make-log-files)
  "
Stores all of the log file streams that are open so that they can be
closed later on. There are potentially far too many of them for
with-open-file.
")

(defconstant +inactivity-time+ (* 5 60)
  "
If a log file has not been written to after this number, in seconds,
then it is closed.
")

;;; If the log file is open, then store the file-stream and the
;;; timestamp of when it was last used. Otherwise, store NIL for the
;;; file-stream and the time is irrelevant.
(defstruct log-file
  (file-stream nil :type (maybe file-stream))
  (time          0 :type (integer 0 #.most-positive-fixnum)))

(define-accessor-macro with-log-file #:log-file-)

;;; Close all file streams.
(defmethod cleanup ((log-files log-files))
  (do-hash-table (key log-file (logs log-files))
    (declare (ignore key))
    (when (log-file-p log-file)
      (with-log-file (file-stream time) log-file
        (when file-stream
          (close file-stream)
          (psetf file-stream nil
                 time 0))))))

(define-function (close-log-file-if-inactive :inline t) ((log-file log-file))
  "Closes a log file stream if more time has passed than +inactivity-time+"
  (with-log-file (file-stream time) log-file
    (let ((inactive? (>= (- (get-universal-time) time) +inactivity-time+)))
      (when inactive?
        (close file-stream)
        (psetf file-stream nil
               time 0))
      inactive?)))

(defun close-inactive-logs ()
  "Closes all inactive log file streams."
  (do-hash-table (key log-file (logs *log-files*))
    (declare (ignore key))
    (when (log-file-p log-file)
      (close-log-file-if-inactive log-file))))

(define-function (update-log-file-time :inline t) ((log-file log-file))
  "Updates the last accessed time for a log file stream."
  (with-log-file (file-stream time) log-file
    (when file-stream
      (setf time (get-universal-time)))))

(define-function (open-log-file-if-necessary :inline t) ((log-file log-file)
                                                         (filename pathname))
  "Opens a log file stream if it is not already open."
  (with-log-file (file-stream time) log-file
    (let ((necessary? (not file-stream)))
      (when necessary?
        (psetf file-stream (open filename
                                 :direction :output
                                 :element-type 'character
                                 :if-exists :append
                                 :if-does-not-exist :create)
               time (get-universal-time)))
      (if necessary? log-file nil))))

(define-function create-log-if-necessary ((filename pathname) &key open?)
  "
Creates a log file if it doesn't already exist. Otherwise, it opens
its log file stream if it's not already open.
"
  (if (hash-table-value-present? (pathname-name filename) (logs *log-files*))
      nil
      (setf (gethash (pathname-name filename) (logs *log-files*))
            (if open?
                (open-log-file-if-necessary (make-log-file) filename)
                (make-log-file)))))

(defun ensure-log-directory-exists (pathname)
  "
Ensures that the directory used by the log files already exists.
"
  (uiop:ensure-all-directories-exist (list (uiop:subpathname (user-homedir-pathname)
                                                             pathname
                                                             :type :directory))))
