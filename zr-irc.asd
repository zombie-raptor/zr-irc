;;; Requires an ASDF version with package-inferred-system
(cl:unless (asdf:version-satisfies (asdf:asdf-version) "3.1.2")
  (cl:error "Zombie Raptor IRC requires ASDF 3.1.2 or later."))

(asdf:defsystem #:zr-irc
  :description "IRC for the Zombie Raptor game engine."
  :version "0.0.0.0"
  :author "Michael Babich"
  :maintainer "Michael Babich"
  :license "MIT"
  :homepage "https://zombieraptor.com/"
  :bug-tracker "https://gitlab.com/zombie-raptor/zr-irc/issues"
  :source-control (:git "https://gitlab.com/zombie-raptor/zr-irc.git")
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :depends-on (:alexandria
               :bordeaux-threads
               :uiop
               :usocket
               :zr-utils
               :zr-irc/all)
  :in-order-to ((asdf:test-op (asdf:test-op "zr-irc/tests"))))

(asdf:defsystem #:zr-irc/tests
  :description "The tests for ZR IRC"
  :author "Michael Babich"
  :maintainer "Michael Babich"
  :license "MIT"
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :depends-on (:fiveam
               :zr-utils)
  :perform (asdf:test-op (o s) (uiop:symbol-call :fiveam
                                                 :run!
                                                 (cl:intern (cl:symbol-name '#:zr-irc/tests)
                                                            '#:zr-irc/tests))))

(asdf:register-system-packages :fiveam :it.bese.fiveam)
