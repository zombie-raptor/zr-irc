(cl:defpackage #:zr-irc/util
  (:use #:cl
        #:zr-utils)
  (:export #:float-time
           #:message-prefix-match?
           #:next-separator-position
           #:non-negative
           #:skip-extra-spaces
           #:with-array-accessors*
           #:write-date
           #:write-datetime
           #:write-ip
           #:write-time))

(cl:in-package #:zr-irc/util)

;;;; IP

(defun write-ip (ip &key port (log *standard-output*) full?)
  (typecase ip
    ;; IPv4
    ((simple-array octet (4))
     (format log "~D.~D.~D.~D" (aref ip 0) (aref ip 1) (aref ip 2) (aref ip 3))
     (when port
       (format log ":~D" port)))
    ;; IPv6
    ((simple-array octet (16))
     (when port
       (write-char #\[ log))
     (if full?
         ;; Note: When full, the leading 0s are printed in each hex
         ;; byte. Otherwise, they are dropped.
         (loop :for byte :across ip
               :for i :from 0
               :do
                  (format log "~4,'0X" byte)
                  (unless (= i 15)
                    (write-char #\: log)))
         ;; Tally the consecutive zeros. The longest subsequence of
         ;; zeros is printed as ::
         ;;
         ;; This algorithm implements the printable representation
         ;; described on Wikipedia at
         ;; https://en.wikipedia.org/wiki/IPv6_address#Representation
         (let ((consecutive-zeros (make-array 16 :element-type '(integer 0 16) :initial-element 0)))
           (declare (dynamic-extent consecutive-zeros))
           ;; Prints how many consecutive zeros there are
           ;; left-to-right (which requires iterating right-to-left).
           ;; The largest possible number is 16 (all zeros).
           (loop :for i :downfrom 15 :to 0
                 :with current-zeros := 0
                 :do (if (zerop (aref ip i))
                         (progn
                           (incf current-zeros)
                           (setf (aref consecutive-zeros i) current-zeros))
                         (progn
                           (setf current-zeros 0
                                 (aref consecutive-zeros i) 0))))
           ;; Iterate through, printing hex digits like normal except
           ;; when it is at the position of the maximum consecutive
           ;; zeros. If that's the case, then it skips.
           (loop :for i :from 0 :below (length ip)
                 :with skip-start := (position (reduce #'max consecutive-zeros)
                                               consecutive-zeros)
                 :with skip-until :of-type (maybe (integer 0 16)):= nil
                 :do
                    (unless (and skip-until (< i skip-until))
                      (if (and skip-start
                               (= i skip-start)
                               (> (aref consecutive-zeros i) 1))
                          (progn
                            (setf skip-until (+ i (aref consecutive-zeros i)))
                            ;; If the string starts with 0s, then this
                            ;; ensures a leading :: to start the
                            ;; printable representation. Otherwise,
                            ;; all :s are trailing a digit.
                            (when (zerop i)
                              (write-char #\: log))
                            (write-char #\: log))
                          (progn
                            (format log "~X" (aref ip i))
                            (unless (= 15 i)
                              (write-char #\: log))))))))
     (when port
       (format log "]:~D" port)))
    ;; Assumed to be an address instead of an IP, but it could really
    ;; be anything at this point.
    (t
     (format log "~A" ip)
     (when port
       (format log " port ~D" port)))))

;;;; Datetime

;;; Note: (+ unix-time +unix-time-offset+) lets you use
;;; decode-universal-time on Unix time
(defconstant +unix-time-offset+
  (encode-universal-time 0 0 0 1 1 1970 0)
  "The Unix time offset, used by RPL_TOPICWHOTIME (333)")

(define-function (%decode-configurable-time :inline t) (encoded-time utc? unix-time?)
  "
Decodes the encoded time. If no encoded-time is given, it uses the
current universal time. Otherwise, it uses the given time, either in
CL's internal (1900-based) format or in the Unix/POSIX (1970-based)
format.
"
  (cond (encoded-time
         (decode-universal-time (+ encoded-time (if unix-time? +unix-time-offset+ 0))
                                (if utc? 0 nil)))
        (utc?
         (decode-universal-time (get-universal-time) 0))
        (t
         (get-decoded-time))))

(defun write-date (&key (stream *standard-output*) (utc? nil) (unix-time? nil) (encoded-time nil))
  "Writes the date in the style of ISO 8601 or RFC 3339."
  (multiple-value-bind (second minute hour day month year)
      (%decode-configurable-time encoded-time utc? unix-time?)
    (declare (ignore second minute hour))
    (format stream "~D-~2,'0D-~2,'0D" year month day)))

;;; TODO: Optionally include the timezone information.
(defun write-time (&key (stream *standard-output*) (utc? nil) (unix-time? nil) (encoded-time nil))
  "
Writes the time in the style of ISO 8601 or RFC 3339, but without the
timezone at the end.
"
  (multiple-value-bind (second minute hour)
      (%decode-configurable-time encoded-time utc? unix-time?)
    (format stream "~2,'0D:~2,'0D:~2,'0D" hour minute second)))

;;; TODO: Rewrite to use write-date and write-time instead of
;;; duplicating the implementation?
(defun write-datetime (&key (stream *standard-output*) (utc? nil) (unix-time? nil) (encoded-time nil))
  "Writes the date and time in the style of ISO 8601 or RFC 3339."
  (multiple-value-bind (second minute hour day month year day-of-the-week dst? tz)
      (%decode-configurable-time encoded-time utc? unix-time?)
    (declare (ignore day-of-the-week))
    (multiple-value-bind (hour-offset minute-offset) (truncate tz)
      (let ((hour-offset (- hour-offset (if dst? 1 0)))
            (minute-offset (* minute-offset 60)))
        (format stream "~D-~2,'0D-~2,'0DT~2,'0D:~2,'0D:~2,'0D" year month day hour minute second)
        (if (and (zerop hour-offset) (zerop minute-offset))
            (write-char #\Z stream)
            (format stream "~A~2,'0D:~2,'0D" (if (> hour-offset 0) "-" "+") (abs hour-offset) minute-offset))
        nil))))

;;;; Parsing

(define-function (next-separator-position :inline t) ((message string) &optional (start 0))
  (and start (position #\Space message :start start)))

(define-function (skip-extra-spaces :inline t) ((message string) &optional (start 0))
  (position #\Space message :start start :test #'char/=))

;;;; Data structures

;;; TODO: develop and test; replace the old command list queue with a
;;; command buffer queue that is built around queuing into this and
;;; dequeuing from this. Use size 8192, at least initially.
(defmacro define-struct-range-queue (name (item-type item-maximum))
  (with-interned-symbols (queue
                          start
                          end
                          empty?
                          into-sequence
                          from-sequence
                          count
                          (with-queue    name :prefix #:with- :suffix #:-queue)
                          (typed-dequeue name :suffix #:-dequeue)
                          (typed-enqueue name :suffix #:-enqueue))
    (let ((item-maximum-value (if (and (symbolp item-maximum) (constantp item-maximum))
                                  (symbol-value item-maximum)
                                  item-maximum)))
      `(progn
         (define-simple-struct ,name
           (,queue (simple-array ,item-type (,item-maximum-value)))
           (,start (mod ,item-maximum-value))
           (,end (mod ,item-maximum-value))
           (,empty? boolean))
         (define-accessor-macro ,with-queue
             ,(concatenate 'string (symbol-name name) "-"))
         (define-function (,typed-dequeue :inline t)
             ((,name ,name) ,into-sequence ,count)
           (,with-queue (,queue ,start ,end ,empty?) ,name
             (let ((end2 (mod (+ ,start ,count) ,item-maximum)))
               (prog1 (if (< end2 ,start)
                          (progn
                            (replace ,into-sequence
                                     ,queue
                                     :start2 ,start)
                            (replace ,into-sequence
                                     ,queue
                                     :start1 (- ,item-maximum ,start 1)
                                     :end2 end2))
                          (replace ,into-sequence
                                   ,queue
                                   :start2 ,start
                                   :end2 end2))
                 (incf-mod ,start ,item-maximum count)
                 (when (= ,start ,end)
                   (setf ,empty? t))))))
         (define-function (,typed-enqueue :inline t)
             ((,name ,name)  ,from-sequence ,count)
           (,with-queue (,queue ,start ,end ,empty?) ,name
             (let ((end1 (mod (+ ,start ,count) ,item-maximum)))
               (prog1 ,from-sequence
                 (if (< end1 ,start)
                     (progn
                       (replace ,queue
                                ,from-sequence
                                :start1 ,start
                                :end2 (- ,item-maximum ,start))
                       (replace ,queue
                                ,from-sequence
                                :end1 end1
                                :start2 (- ,item-maximum ,start 1)))
                     (replace ,queue
                              ,from-sequence
                              :start1 ,start
                              :end1 end1))
                 (incf-mod ,end ,item-maximum ,count)
                 (when ,empty?
                   (setf ,empty? nil))))))))))

;;;; Misc

(define-function (message-prefix-match? :inline t) ((candidate string)
                                                    (message string)
                                                    &key
                                                    message-start
                                                    (prefix-char nil (maybe character))
                                                    (suffix-char nil (maybe character))
                                                    exact-match?)
  "
Matches the start of a MESSAGE to see if it is CANDIDATE, starting
from the position MESSAGE-START. If EXACT-MATCH? is true, then the
message must end when the match ends. Otherwise, it's just a prefix.
Additionally, a PREFIX-CHAR and/or SUFFIX-CHAR can be provided, if
desired. Usually, one will be provided.
"
  ;; Hide "deleting unreachable code" when prefix-char and/or
  ;; suffix-char are missing.
  (locally (declare #+sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
    (and (if exact-match?
             (= (+ message-start
                   (length candidate)
                   (if prefix-char 1 0)
                   (if suffix-char 1 0))
                (length message))
             (<= (+ message-start
                    (length candidate)
                    (if prefix-char 1 0)
                    (if suffix-char 1 0))
                 (length message)))
         (or (not prefix-char)
             (char= prefix-char
                    (aref message message-start)))
         (string= candidate
                  message
                  :start2 (+ message-start
                             (if prefix-char 1 0))
                  :end2 (+ message-start
                           (if prefix-char 1 0)
                           (length candidate)))
         (or (not suffix-char)
             (char= suffix-char
                    (aref message (+ message-start
                                     (if prefix-char 1 0)
                                     (length candidate))))))))

(define-function (non-negative :inline t) ((n int16))
  (and (not (minusp n)) n))

(defmacro with-array-accessors* (slots &body body)
  `(with-array-accessors ,slots 0 ,@body))

(define-function (float-time :inline t) ()
  (/ (get-internal-real-time)
     (float* internal-time-units-per-second)))
