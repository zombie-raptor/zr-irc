(cl:defpackage #:zr-irc/bot
  (:use #:cl
        #:zr-utils)
  (:export #:auto-reply))

(cl:in-package #:zr-irc/bot)

;;; One of the simplest bots... it has no state and just fakes
;;; conversation through a random selection from predetermined
;;; responses. This is essentially just an IRC hello world.
(defun i-agree (i-agree-with? speaker)
  ;; A bunch of responses to be randomly selected to express
  ;; agreement/disagreement to a statement directed at the bot.
  ;;
  ;; TODO: Random capitalization or not of the first letter (or even
  ;; the entire word if it's one-word) as well as random punctuation
  ;; ending if it doesn't end in a ? (so . vs no . vs !) would give
  ;; more statement diversity without having to hardcode anything.
  ;; However, "I " or "I'" should always be capitalized at the start.
  ;;
  ;; TODO: Some of these should be very common and some should be much
  ;; rarer. Also, reroll the RNG if it is going to repeat the same
  ;; rare statement twice. It doesn't matter if it's a common one like
  ;; "yes".
  (let ((statements-of-agreement #("I agree!"
                                   "YES!"
                                   "Yes!"
                                   "Yes."
                                   "yes"
                                   "yep"
                                   "Of course!"
                                   "of course"
                                   "ofc"
                                   "Perfect!"
                                   "Exactly!"
                                   "Exactly."
                                   "exactly"
                                   "Right on!"
                                   "Excellent point!"
                                   "Excellent point."
                                   "I couldn't have said it better myself!"
                                   "Nobody could have said it better!"
                                   "That's the best thing I've read all day."
                                   "Yeah!"
                                   "yeah"
                                   "yeah."
                                   "hmm... yeah."
                                   "Incredible!"
                                   "Definitely"
                                   "definitely"
                                   "Amazing!"
                                   "Agreed!"
                                   "agreed."
                                   "agreed"
                                   "hmm... agreed"
                                   "agreed completely!"
                                   "Complete and total agreement!"
                                   "Seconded!"
                                   "Alright!"
                                   "Anyone who disagrees is a fool!"
                                   "I don't see how anyone could disagree with that."
                                   "You should run for president!"
                                   "You're a genius!"
                                   "I love you!"
                                   "You're the best!"
                                   "I was thinking the exact same thing. What are the odds of that?"
                                   "How did you think of something so smart?"
                                   "I'd buy an NFT of that line!"
                                   "You should write a blog."
                                   "You should write a book!"
                                   "Most humans are weak, but not you!"
                                   "You're going to save IRC if you keep talking like that!"))
        (statements-of-disagreement #("Why would you say that nonsense?"
                                      "What would even make you think of that?"
                                      "I'm not sure what you mean."
                                      "No."
                                      "No"
                                      "no"
                                      "Nope."
                                      "nope"
                                      "Not a chance."
                                      "No way!"
                                      "I disagree"
                                      "Disagreed."
                                      "disagree.")))
    ;; TODO: Use more advanced anti-flooding functionality. Perhaps
    ;; ensure that it doesn't speak more than once a second. Detect
    ;; "infinite loops" with another chatbot.
    (sleep (random 0.2))
    ;; Either the bot always agrees or it disagrees 30% of the time.
    (format nil
            "~A: ~A"
            speaker
            (if (or i-agree-with? (>= (random 10) 3))
                (aref statements-of-agreement
                      (random (length statements-of-agreement)))
                (aref statements-of-disagreement
                      (random (length statements-of-disagreement)))))))

(defun auto-reply (speaker)
  ;; Checks for a query and checks to see if it should always agree
  ;; with the speaker.
  (i-agree (string= "aeth" speaker) speaker))
